(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "xmls/test")
  (asdf:load-system "xmls/unit-test")
  (asdf:load-system "xmls/octets"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(let ((results (5am:run 'xmls-test::xmls-test)))
  (5am:explain! results)
  (unless (5am:results-status results)
    (uiop:quit 1)))

#-ecl
(unless (xmls::test)
  (print "Failed XMLS test.")
  (uiop:quit 1))

(unless (xmls/octets::test)
  (print "Test failures in XMLS/octets.")
  (uiop:quit 1))
